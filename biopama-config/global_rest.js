/* 
REST  Endpoints
*/
var biopamaApiUrl = 'https://api.biopama.org';
var biopamaRestServices = {
    dopaRadarPlot: "https://rest-services.jrc.ec.europa.eu/services/d6dopa40/protected_sites/get_wdpa_terrestrial_radarplot?format=json&wdpaid=",
    dopaCountryProtCon: "https://rest-services.jrc.ec.europa.eu/services/d6dopa40/administrative_units/get_country_all_inds?format=json&fields=area_terr_km2,area_terr_perc,area_mar_km2,area_mar_perc,area_prot_km2,area_prot_perc,area_prot_terr_km2,area_prot_terr_perc,area_prot_mar_km2,area_prot_mar_perc,protconn&b_iso2=",
    dopaPaNums: "https://rest-services.jrc.ec.europa.eu/services/d6dopa40/administrative_units/get_country_pa_count?format=json&includemetadata=false&b_iso2=",
    dopaAvgClimate: "https://rest-services.jrc.ec.europa.eu/services/d6dopa40/climate/get_worldclim_pa?format=json&wdpaid=",
    biopamaCountryBounds: "https://geospatial.jrc.ec.europa.eu/geoserver/wfs?request=getfeature&version=1.0.0&service=wfs&typename=biopama%3AEEZ_GAUL_DIISSOLVED&propertyname=iso2,&SORTBY=iso2&CQL_FILTER=iso2=%27[TOKEN]%27&outputFormat=application%2Fjson",
    biopamaPaBounds: "https://geospatial.jrc.ec.europa.eu/geoserver/wfs?request=getfeature&version=1.0.0&service=wfs&typename=marxan:wdpa_latest_biopama&propertyname=wdpaid,name,&SORTBY=wdpaid&CQL_FILTER=wdpaid=[TOKEN]&outputFormat=application%2Fjson",
    countryISO3bounds: biopamaApiUrl+'/api/pame/function/api_bbox_for_countries_dateline_safe/iso3codes=',
}