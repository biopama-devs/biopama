var biopamaPAColors = {
    totalArea: '#bab9b7',
    terrestrial: '#8FBF4B',
    marine: '#8eb4b1',
    costal: '#df772c',
    noColor: '#353a3e',
    protConn: 'rgb(127,173,65)',
    chartText: 'rgb(128 128 128)',
};

var biopamaGlobal = {
    geonodeURL: "https://geonode-rris.biopama.org/geoserver/geonode/wms?",
    map: {
        //mapboxAccessToken: 'pk.eyJ1IjoiYmxpc2h0ZW4iLCJhIjoiMEZrNzFqRSJ9.0QBRA2HxTb8YHErUFRMPZg', //for any layers that might come from mapbox
        mapboxAccessToken: 'pk.eyJ1IjoiamFtZXNkYXZ5IiwiYSI6ImNpenRuMmZ6OTAxMngzM25wNG81b2MwMTUifQ.A2YdXu17spFF-gl6yvHXaw', //for any layers that might come from mapbox
        hostURL: {
            poly: "https://tiles.biopama.org/BIOPAMA_poly",
            point: "https://tiles.biopama.org/BIOPAMA_point",
        },
        baseLayerStyle: 'dark-v10', //the name of the base layer style provided by mapbox https://www.mapbox.com/maps
        center:  [60, -6.66],
        zoom: 1.6,
        attribution: "UNEP-WCMC and IUCN (2022), <br>\n Protected Planet: The World Database on Protected Areas (WDPA),<br>\n June 2022, Cambridge, UK: UNEP-WCMC and IUCN.",
        layers: { //layernames from tippecanoe
            nonACPCountry: "non_acp_countries",
            country: "ACP_Countries",
            countryPoint: "ACP_Countries_points",
            pa: "ACPWDPAPolyJun2022",
            paLabels: "ACPWDPAPolyLabelsJun2022",
            paPoint: "ACPWDPAPolyLabelsJun2022",
            region: "ACP_Groups",
            subRegion: "ACP_SubGroups",
            regionPoint: "ACP_Groups_points",
            subRegionPoint: "ACP_SubGroups_points",
            EEZ: "ACP_EEZ",
            GAUL: "ACP_GAUL",
            WDPALatest: "wdpa_latest_biopama", //new geoserver mapLayer
        },
        colors:{
            region: "#a25b28",
            country: "#679B95",
            pa: "#8FBF4B",
        }
    },
    chart: {
        colors: {
            default: ['#8BC34A','#e97a1b','#ecebeb','#179dc7'],
            twoTerrestrial: [biopamaPAColors.terrestrial, biopamaPAColors.noColor],
            twoMarine: [biopamaPAColors.marine, biopamaPAColors.noColor],
            twoCoastal: [biopamaPAColors.costal, biopamaPAColors.noColor],
            ProtConn: [biopamaPAColors.protConn, biopamaPAColors.noColor],
            twoGood: ['#000', biopamaPAColors.noColor], // TODO change to nice green shade (seperate to terrestrial?)
            twoBad: ['#000', biopamaPAColors.noColor], // TODO change to nice red shade
            background: '#2c343c',
            text: biopamaPAColors.chartText,
        },
        toolbox: {
            show: true,
            feature: {
                restore: {
                    title: 'Restore'
                },
                saveAsImage: {
                    title: 'Image',
                    name: "Indicator_chart"
                }
            }
        },
        legend: {
            show: true,
            orient: 'horizontal',
            align: 'left',
            top: '2%',
            left: '5%',
            width: '80%',
            itemGap: 4,
            itemHeight: 7,
            itemWidth: 15,
            padding: 1,
            textStyle: {
                color: biopamaPAColors.chartText,
            } 
        },
    },
    regions: [
        {
            id: "ACP",
            name: "ACP",
            path: null,
            isoCodes: ['BW', 'RA'],
        },
        {
            id: "EAF",
            name: "Eastern Africa",
            path: "east_africa",
        },
        {
            id: "SAF",
            name: "Southern Africa",
            path: "south_africa",
        },
        {
            id: "WAF",
            name: "Western Africa",
            path: "west_africa",
        },
        {
            id: "CAF",
            name: "Central Africa",
            path: "central_africa",
        },
        {
            id: "CAR",
            name: "ACP Caribbean",
            path: "caribbean",
        },
        {
            id: "PAC",
            name: "ACP Pacific",
            path: "pacific",
        },
    ], 
};
