/**
 * @file
 * Indicator Card custom JS.
 *
 */
//var mymap;

function addIndicator() {
    var $ = jQuery;
    var nid = $(".layer_nid").text().trim();
    var url = '/wms_layers/'+nid;
    //console.log(url)
    //mymap = map;
    $.ajax({
        url: url,
        dataType: 'json',
        success: function(d) {
            info = d[0];
            //console.log(d)
            //attach(info);
            if (info.field_layer_server_type === "Geoserver") {
                //console.log(mymap)
                // map.addLayer({
                //     'id': info.field_wms_layer_id,
                //     'type': 'raster',
                //     'source': {
                //         'id': info.field_wms_layer_id,
                //         'type': 'raster',
                //         'tiles': [ info.field_wms_base_url + 'SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&' + 
                //         'layers=' + info.field_wms_layer_name + '&' +
                //         'transparent=true&'+
                //         'format=image/png&'+
                //         'styles=' + info.field_wms_style + '&' +
                //         'opacity=1&' +
                //         'HEIGHT=256&WIDTH=256&SRS=EPSG:3857&BBOX={bbox-epsg-3857}' ],
                //         'tileSize': 256,
                //         'scheme': 'xyz',
                //     },
                //     'paint': {"raster-opacity" : .05}
                // });
                
            } else if (info.field_layer_server_type === "gbif") {
                objs[info.field_wms_layer_id] = L.tileLayer(info.field_wms_base_url, {
                    minZoom: 1,
                    maxZoom: 17,
                    zoomOffset: -1,
                    tileSize: 512,
                    zIndex: 32
                });
            } else if (info.field_layer_server_type === "Python") {
                objs[info.field_wms_layer_id] = L.tileLayer.wms(info.field_wms_base_url, {
                    layers: info.field_wms_layer_name,
                    transparent: true,
                    format: 'image/png',
                    time: '2010',
                    styles: info.field_wms_style,
                    opacity: '1',
                    zIndex: 32
                });
            } else if (info.field_layer_server_type === "ESRItiles") {
                objs[info.field_wms_layer_id] = L.tileLayer(info.field_wms_base_url, {
                    zIndex: 32
                });
            } else if (info.field_layer_server_type === "ESRI") {
                objs[info.field_wms_layer_id] = L.esri.imageMapLayer({
                    url: info.field_wms_base_url,
                    zIndex: 1,
                    token: 'ZKRjsVzCgYCalBO_-aSIyFJc2ukLchzFoJb_7VcMYjsyQKlGA0reYZHJxWkUykmH8UfnLDqqlOtATQ9-zKxwLo5efjE_HWerTq3JFq7GpeosvgjRVXGmqamKFhSlb7hL0D9I2gt8d2sksUCPo_xsAyGwW1re5hOgFg7VkNt1pcsqMSuyAu58I7B5uHug5JW6KK4ppaEn3LIZLQmKEoS85x-6djnrLTTbDbb8l4zlbhw.',
                    attribution: 'Map provided by ESRI',
                    opacity: '0.5'
                });
            } else if (info.field_layer_server_type === "Floods") {

                var today_floods = new Date(); //floods
                var dd_floods = today_floods.getDate();
                var mm_floods = today_floods.getMonth() + 1;
                var yyyy_floods = today_floods.getFullYear();
                if (mm_floods < 10) { mm_floods = '0' + mm_floods }
                today_floods = yyyy_floods + '-' + mm_floods + '-' + dd_floods;

                objs[info.field_wms_layer_id] = L.tileLayer.wms('' + info.field_wms_base_url + '', {
                    layers: info.field_wms_layer_name0,
                    transparent: true,
                    format: 'image/png',
                    crs: L.CRS.EPSG4326,
                    MAP_RESOLUTION: '180',
                    time: today_floods,
                    styles: info.field_wms_style,
                    version: '1.3.0',
                    opacity: '1',
                    zIndex: 80
                });
            } else if (info.field_layer_server_type === "ESTATION") {
                objs[info.field_wms_layer_id] = L.tileLayer.wms('' + info.field_wms_base_url + '', {
                    layers: info.field_wms_layer_name,
                    transparent: true,
                    format: 'image/png',
                    crs: L.CRS.EPSG4326,
                    styles: info.field_wms_style,
                    opacity: '1',
                    zIndex: 32
                });
            } else if (info.field_layer_server_type === "Edo") {
                objs[info.field_wms_layer_id] = L.tileLayer.wms('' + info.field_wms_base_url + '', {
                    layers: info.field_wms_layer_name,
                    SRS: 'EPSG:4326',
                    format: 'image/png',
                    SELECTED_YEAR:'2021',
                    SELECTED_MONTH:'12',
                    SELECTED_TENDAYS:'21',
                    crs: L.CRS.EPSG4326,
                    zIndex: 32,
                    transparent: true,
                    version: '1.1.1',
                    opacity: 1
                });
            } else if (info.field_layer_server_type === "spi") {

                var today_edo = new Date(); //drought
                var dd_edo = today_edo.getDate();
                var mm_edo =  (today_edo.getMonth()); //January is 0!
                var yyyy_edo = today_edo.getFullYear();
                if (dd_edo <= 10) { dd_edo = '01' } else if (dd_edo >= 11 && dd_edo <= 19) { dd_edo = '11' } else if (dd_edo >= 20) { dd_edo = '21' } else {}
                if (mm_edo < 10) { mm_edo = '0' + mm_edo }
                if (mm_edo == 0) { mm_edo = 12 }
                today_edo = yyyy_edo + '-' + (mm_edo) + '-' + dd_edo;
                var mm_spi = mm_edo - 1
                var mm_edo = mm_edo 
                today_spi = yyyy_edo + '-' + '0' + mm_spi;

                objs[info.field_wms_layer_id] = L.tileLayer.wms('' + info.field_wms_base_url + '', {
                    layers: info.field_wms_layer_name,
                    SRS: 'EPSG:4326',
                    format: 'image%2Fpng',
                    SELECTED_TIMESCALE:'03',
                    TIME: today_spi,
                    crs: L.CRS.EPSG4326,
                    zIndex: 32,
                    transparent: true,
                    version: '1.1.1',
                    opacity: 1
                });
            } else if (info.field_layer_server_type === "NASA") {
                objs[info.field_wms_layer_id] = L.tileLayer.wms('' + info.field_wms_base_url + '', {
                    layers: info.field_wms_layer_name,
                    transparent: true,
                    format: 'image/png',
                    styles: info.field_wms_style,
                    opacity: '1',
                    zIndex: 32
                });
            } else if (info.field_layer_server_type === "Geoserver_popup") {
                objs[info.field_wms_layer_id] = L.tileLayer.betterWms('' + info.field_wms_base_url + '/wms', {
                    layers: info.field_wms_layer_name,
                    transparent: true,
                    format: 'image/png',
                    styles: info.field_wms_style,
                    opacity: '1',
                    zIndex: 32
                });
            } else if (info.field_layer_server_type === "GeoWebCache") {
                // objs[info.field_wms_layer_id] = L.tileLayer('' + info.field_wms_base_url + '/gwc/service/tms/1.0.0/' + info.field_wms_layer_name + '@EPSG:900913@png/{z}/{x}/{y}.png', {
                //     tms: true,
                //     zIndex: 32,
                //     opacity: '1'
                // });
            } else if (info.field_layer_server_type === "Google") {
                objs[info.field_wms_layer_id] = L.tileLayer('https://storage.googleapis.com/' + info.field_wms_layer_name + '/{z}/{x}/{y}.png', {
                    format: "image/png",
                    zIndex: 32,
                    opacity: '1'
                });
            } else if (info.field_layer_server_type === "GoogleEarthEngine") {
                objs[info.field_wms_layer_id] = L.tileLayer('https://earthengine.googleapis.com/v1alpha/projects/earthengine-legacy/maps/' + info.field_wms_layer_name + '/tiles/{z}/{x}/{y}', {
                    format: "image/png",
                    zIndex: 32,
                    opacity: '1'
                });
            } else if (info.field_layer_server_type === "coralreefwatch") {
                objs[info.field_wms_layer_id] = L.tileLayer('https://coralreefwatch.noaa.gov/data1/vs/google_maps/' + info.field_wms_layer_name + '/{z}/{x}/{y}.png', {
                    opacity: 1,
                    transparent: true,
                    zIndex: 39,
                    tms: true
                });
            } else if (info.field_layer_server_type === "Jeodpp") {
                objs[info.field_wms_layer_id] = L.tileLayer('' + info.field_wms_base_url + '/services/ows/ts/' + info.field_wms_layer_name + '', {
                    format: "image/png",
                    crs: L.CRS.EPSG4326,
                    zIndex: 32,
                    opacity: '1'
                });

            } else if (info.field_layer_server_type === "UNLAB") {
                objs[info.field_wms_layer_id] = L.tileLayer(info.field_wms_base_url, {
                    format: "image/png",
                    zIndex: 32,
                    opacity: '1'
                });
            } else if (info.field_layer_server_type === "Carto") {
                    objs[info.field_wms_layer_id] = L.tileLayer('' + info.field_wms_base_url + 'api/v1/map/' + info.field_wms_layer_name + '/{z}/{x}/{y}.png', {
                        format: "image/png",
                        zIndex: 32,
                        opacity: '1'
                    });
            } else if (info.field_layer_server_type === "timeseries") {
                objs[info.field_wms_layer_id] = L.tileLayer.wms('' + info.field_wms_base_url + '', {
                    layers: info.field_wms_layer_name,
                    SRS: 'EPSG:4326',
                    format: 'image/png',
                    styles: info.field_wms_style,
                    time: info.field_wms_time,
                    zIndex: 32,
                    transparent: true,
                    version: '1.1.1',
                    opacity: 1
                });
            } else {
                return;
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });

    //--------------------------------------------------------------------------------------------
    // LOOP THROUGH LAYERS
    //--------------------------------------------------------------------------------------------
    function attach(props) {

        var layerID = props.field_wms_layer_id;
        var title = props.title;
        var legend = props.field_layer_legend;
        var stats_legend = props.field_layer_legend_stats;

        const alertPlaceholder = document.getElementById('AlertPlaceholder'+nid);
        console.log('AlertPlaceholder'+nid);
        console.log(alertPlaceholder);

        const alert = (message, type) => {
            const wrapper = document.createElement('div')
            wrapper.innerHTML = [
              `<div class="alert alert-${type} alert-dismissible fade show" role="alert">`,
              `   <div>${message}</div>`,
              '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
              '</div>'
            ].join('')
          
            alertPlaceholder.append(wrapper)
        }

        const alertTrigger = document.getElementById('downloadStatistics'+nid);
        if (alertTrigger) {
            alertTrigger.addEventListener('click', () => {
                alert('Statistics not available for this layer.', 'warning');
            })
        }

        $('#' + layer_id).unbind('click');
        $('#' + layer_id).click(function() {
            if (map.hasLayer(objs[layer_id])) {
                map.removeLayer(objs[layer_id]);
                $('#' + layer_id).html('<i class="material-icons">layers</i>');
                $('#' + layer_id).attr('style', 'background-color: #ffc107 !important');
                $('#' + layer_id + '_legend').remove();

                if (layer_id == '.') {
                    console.info(carbon_layerGroup)
                    carbon_layerGroup.clearLayers();
                }
            } else {
                map.addLayer(objs[layer_id]);

                $('#' + layer_id).html('<i class="material-icons">layers_clear</i>');
                $('#' + layer_id).attr('style', 'background-color: #b9c3ce !important');

                // countries.setParams({ styles:'white_polygon_t'});
                $('.all_stats_legend').empty();
                // countries.setParams({CQL_FILTER:"iso2 LIKE '%%'"});


                $('#legends').prepend('<div class= "legend_container" id ="' + layer_id + '_legend"><p>' + title + '</p>' + legend + '<div class = "slider_legend"><input type="text" value="100" id="slider_' + layer_id + '" data-slider-min="0" data-slider-max="100" data-slider-step="5" data-slider-value="100"></div><em class = "tools_legend_content"><i class="btn-small tooltipped material-icons flip_to_front flip_to_front_' + layer_id + '" data-position="top" data-tooltip="To top">flip_to_front</i><i class="btn-small tooltipped material-icons bar_chart bar_chart_' + layer_id + '" data-position="bottom" data-tooltip="Perform statistics">bar_chart</i><i class="btn-small tooltipped material-icons cloud_download cloud_download_' + layer_id + '" data-position="bottom" data-tooltip="Download statistics" >cloud_download</i><i class=" material-icons close close_' + layer_id + '" >close</i><i id = "visibility_layer_' + layer_id + '" class=" material-icons visibility_on hide_' + layer_id + '" >visibility_off</i><div class="refresh_legend_' + layer_id + '"></div></em><div id = "' + layer_id + '_stats_legend" class = "all_stats_legend"></div></div>')
                var sliderVal;
                $(function() {
                    $('#slider_' + layer_id + '').bootstrapSlider().on('slide', function(ev) {
                        sliderVal = ev.value;
                        objs[layer_id].setOpacity(sliderVal / 100);
                        countries.setOpacity(sliderVal / 100);
                    });
                    if (sliderVal) {
                        $('#slider_' + layer_id + '').bootstrapSlider('setValue', sliderVal);
                    }
                });

                // Time series for Year and Month and Day -----------------------------------------------

                $('#drop_year_layer_'+layer_id).on('change', function() {
                    var  year_ts = ($('#drop_year_layer_'+layer_id).val());
                    var  month_ts = ($('#drop_month_layer_'+layer_id).val());
                    var  day_ts = ($('#drop_day_layer_'+layer_id).val());
                    (objs[layer_id]).setParams({ time: year_ts+'-'+month_ts+'-'+day_ts});
                    
                });
                $('#drop_month_layer_'+layer_id).on('change', function() {
                    var  year_ts = ($('#drop_year_layer_'+layer_id).val());
                    var  month_ts = ($('#drop_month_layer_'+layer_id).val());
                    var  day_ts = ($('#drop_day_layer_'+layer_id).val());
                    (objs[layer_id]).setParams({ time: year_ts+'-'+month_ts+'-'+day_ts});
                });
                $('#drop_day_layer_'+layer_id).on('change', function() {
                    var  year_ts = ($('#drop_year_layer_'+layer_id).val());
                    var  month_ts = ($('#drop_month_layer_'+layer_id).val());
                    var  day_ts = ($('#drop_day_layer_'+layer_id).val());
                    (objs[layer_id]).setParams({ time: year_ts+'-'+month_ts+'-'+day_ts});
                });

                // end of Time series for Year and Month and Day -----------------------------------------------


                // Time series ACLED Armed Conflicts -----------------------------------------------

                $('#drop_year_layer_acled').on('change', function() {
                    var  year_ts = ($('#drop_year_layer_acled').val());
                    var  month_ts = ($('#drop_month_layer_acled').val());
                    var cqlfilteracled = "event_date ILIKE '"+'%'+ month_ts+' '+year_ts + "'";
                    (objs[layer_id]).setParams({ CQL_FILTER: cqlfilteracled });
                    console.log(cqlfilteracled)
                    
                });
                $('#drop_month_layer_acled').on('change', function() {
                    var  year_ts = ($('#drop_year_layer_acled').val());
                    var  month_ts = ($('#drop_month_layer_acled').val());
                    var cqlfilteracled = "event_date ILIKE '"+'%'+ month_ts+' '+year_ts + "'";
                    (objs[layer_id]).setParams({ CQL_FILTER: cqlfilteracled });
                    console.log(cqlfilteracled)
                });


                //  European News EMM -----------------------------------------------

                if (layer_id == 'emmid') {
                    carbon_layerGroup = new L.LayerGroup();
                    $("#emm_but").click(function(){
                        var str = $("#emm_val").val();
                        $("#emm_out").html(str);
                        carbon_layerGroup.clearLayers();
                        $.get('https://proxy.biopama.org/emm.newsbrief.eu/rss/rss?language=en&type=search&mode=advanced&all='+str).done(function(xml) {
                            var output = {}; // restructure the xml as an object
                            
                            $(xml).find('item').each(function() {
                                var nodes = $(this).children();
                                $.each(nodes, function(i, node) {
                                    var name = node.tagName;
                                    var value = node.textContent;
                                    output[name] = value;
                                });
                                var marker = new L.marker([output['georss:point'].split(' ')[0], output['georss:point'].split(' ')[1]]);
                                var popupContent = ('<div id ="pop_emm"><h6>' + output.title + '</h6><i>' + output.pubDate + '</i><br><br><a href="'+output.link+'" target="_blank">Read more</a><i></div>');
                                marker.bindPopup(popupContent)
                                setTimeout(function() {
                                carbon_layerGroup.addLayer(marker)
                                }, 100)
                            });
                            setTimeout(function() {
                            carbon_layerGroup.addTo(map);
                            $('#emm_val').attr("placeholder", "Type a keyword");

                            }, 100)
                        })
                    });

                    $.get('https://proxy.biopama.org/emm.newsbrief.eu/rss/rss?language=en&type=search&mode=advanced&all=AFRICA').done(function(xml) {
                        var output = {}; // restructure the xml as an object
                        $(xml).find('item').each(function() {
                            var nodes = $(this).children();
                            $.each(nodes, function(i, node) {
                                var name = node.tagName;
                                var value = node.textContent;
                                output[name] = value;
                            });
                    
                            var marker = new L.marker([output['georss:point'].split(' ')[0], output['georss:point'].split(' ')[1]]);
                            var popupContent = ('<div id ="pop_emm"><h6>' + output.title + '</h6><i>' + output.pubDate + '</i><br><br><a href="'+output.link+'" target="_blank">Read more</a><i></div>');
                            marker.bindPopup(popupContent)
                            carbon_layerGroup.addLayer(marker)
                        });
                        carbon_layerGroup.addTo(map);
                        setTimeout(function() {
                            $('#emm_val').attr("placeholder", "Type a keyword");
                        }, 100)
                    })
                }
            }
            //  European News EMM -----------------------------------------------


            //  Legend Setup -----------------------------------------------
            $('#legends').click(function(e) {
                if ($(e.target).hasClass('close')) {
                    var container = $(e.target).parents().closest('.legend_container');
                    container.remove();
                    var container_id = container.attr('id').split('_legend')[0];
                    map.removeLayer(objs[container_id]);
                    $('#' + container_id).html('<i class="material-icons">layers</i>');
                    $('#' + container_id).attr('style', 'background-color: #ffc107 !important');
                    countries.setParams({ CQL_FILTER: "iso2 LIKE '%%'" });
                }
            })

            // flip to front -----------------------------------------------

            setTimeout(function() {
                $('.flip_to_front_' + layer_id + '').click(function() {
                        $('.tooltipped').tooltip('close');
                    setTimeout(function() {
                        map.removeLayer(objs[layer_id]);
                    }, 100);
                    setTimeout(function() {
                        map.addLayer(objs[layer_id]);
                    }, 200);
                        setTimeout(function() {
                            $('#' + layer_id + '').click();
                        }, 1000);
                        setTimeout(function() {
                            $('#' + layer_id + '').click();
                        }, 2000);
                        countries.setParams({ styles: 'white_polygon_t' });
                        $('.all_stats_legend').empty();
                });

                // remove layer from legend -----------------------------------------------

                $('.close_' + layer_id + '').click(function() {
                    $('#' + layer_id + '').click();
                    $('#' + layer_id + '_legend').remove();
                    
                    countries.setParams({ styles: 'white_polygon_t' });
                });

                // hide layer from legend -----------------------------------------------

                $('.hide_' + layer_id + '').click(function() {
                    if ($('#visibility_layer_' + layer_id + '').hasClass("visibility_on")) {
                        $('#visibility_layer_' + layer_id + '').toggleClass('visibility_on visibility_off');
                        objs[layer_id].setOpacity(0);

                        console.warn('visible')
                    } else {
                        $('#visibility_layer_' + layer_id + '').toggleClass('visibility_off visibility_on');
                        objs[layer_id].setOpacity(1);

                        console.warn('invisible')
                    }
                });

                // download ststistics -----------------------------------------------
                $('.cloud_download_' + layer_id + '').click(function() {

                    if (stats_legend == "Statistics not yet available") {
                        //M.toast({ html: 'Statistics not available for this layer.' })
                    } else {
                        window.location = 'https://geospatial.jrc.ec.europa.eu/geoserver/wfs?request=getfeature&version=1.0.0&service=wfs&typename=africa_platform:ap_country_stats&propertyname=name,iso2,iso3,' + layer_id + '&SORTBY=iso2&outputFormat=csv'
                    }
                });

                // create country statistics -----------------------------------------------
                setTimeout(function() {
                    $('.bar_chart_' + layer_id).unbind('click');
                    $('.bar_chart_' + layer_id).click(function() {
                        if (countries.wmsParams.styles !== 'ap_' + layer_id + '') {
                            countries.setParams({ styles: 'ap_' + layer_id + '' });
                            countries.setParams({ CQL_FILTER: "iso2 LIKE '%%'" });
                            setTimeout(function() {
                                $('#' + layer_id + '_stats_legend').html(stats_legend)

                                setTimeout(function() {
                                    var country_stats_rest = 'https://geospatial.jrc.ec.europa.eu/geoserver/wfs?request=getfeature&version=1.0.0&service=wfs&typename=africa_platform:ap_country_stats&propertyname=name,iso2,iso3,cropid,lpid,agbid,popdensid,forestlossid,forestgainid,forestcoverid,waternetchangeid,totprotectionid,urbanrateid,soilid,biohotid,wwfterpriid,naturalid,eventsid,hotspotfreqid,rangelandid,warningcid,warningrid,tradesexportid,tradesimportid,klcid,chinaprojid,reservoiresid,watertransitionid,wetlandsid,oilpalmid,trophicid,turbidityid&SORTBY=iso2&outputFormat=application/json&CQL_FILTER=iso2%3E%27AO%27'; //get get_pa_water_stats
                                    $.ajax({
                                        url: country_stats_rest,
                                        dataType: 'json',
                                        success: function(d) {
                                            if (d.totalFeatures == 0) {} else {
                                                var iso2 = [];
                                                var name = [];
                                                var bbox = [];
                                                var indicator = [];
                                                var arr_data = [];

                                                $(d.features).each(function(i, data) {
                                                    var obj = {};
                                                    var properties = data.properties;
                                                    for (var prop in properties) {
                                                        if (prop == layer_id) {
                                                            indicator.push(properties[prop]);
                                                            obj.y = properties[prop];
                                                        } else if (prop == 'iso2') {
                                                            iso2.push(properties[prop]);
                                                            obj.iso2 = properties[prop];
                                                        } else if (prop == 'name') {
                                                            name.push(properties[prop]);
                                                            obj.name = properties[prop];
                                                        } else if (prop == 'bbox') {
                                                            bbox.push(properties[prop]);
                                                            obj.bbox = properties[prop];
                                                        } else {}
                                                    }
                                                    arr_data.push(obj);
                                                });
                                                arr_data.sort(function(a, b) {
                                                    return a.y - b.y;
                                                })
                                                var categories = [];

                                                for (var p in arr_data) {
                                                    categories.push(arr_data[p].name);
                                                };
                                                $('#' + layer_id + '_stats_legend').append('<div id ="chartDiv"></div> ');

                                                if (arr_data[arr_data.length - 1].y > 0) {

                                                    $('#chartDiv').highcharts({
                                                        chart: {
                                                            type: 'column',
                                                            events: {
                                                                load: function() {
                                                                    var rainbow = new Rainbow();
                                                                    // Set start and end colors
                                                                    rainbow.setSpectrum('#ffffff', '#ffffff');

                                                                    // Set the min/max range
                                                                    var theData = this.series[0].data;
                                                                    var rangeLow = 0;
                                                                    var rangeHigh = theData.length
                                                                    rainbow.setNumberRange(rangeLow, rangeHigh);

                                                                    // Loop over data points and update the point.color value
                                                                    for (index = 0; index < theData.length; ++index) {

                                                                        this.series[0].data[index].update({
                                                                            color: '#' + rainbow.colourAt(index)
                                                                        });
                                                                    }
                                                                }
                                                            },
                                                            zoomType: 'xy',
                                                            height: 280,
                                                            backgroundColor: 'rgba(0,0,0,0)'
                                                        },
                                                        title: {
                                                            text: null
                                                        },
                                                        credits: {
                                                            enabled: false,
                                                        },
                                                        xAxis: {
                                                            categories: categories
                                                        },
                                                        yAxis: {
                                                            title: {
                                                                text: ''
                                                            }
                                                        },
                                                        tooltip: {
                                                            hideDelay: 500,
                                                            useHTML: true,
                                                            pointFormat: '<b>{point.y:.2f}</b><hr>',
                                                            style: {
                                                                pointerEvents: 'auto'
                                                            },
                                                            shared: true
                                                        },
                                                        plotOptions: {
                                                            column: {
                                                                pointPadding: 0.2,
                                                                borderWidth: 0,
                                                                turboThreshold: 20000,
                                                                colorByPoint: true
                                                            }
                                                        },
                                                        series: [{
                                                            cursor: 'pointer',
                                                            showInLegend: false,
                                                            //color: '#f6ac07',
                                                            data: arr_data,
                                                            point: {
                                                                events: {
                                                                    click: function() {
                                                                        var iso2filter = this.category

                                                                        // var filterchart="name<>'"+iso2filter+"'";
                                                                        //  countries.setParams({CQL_FILTER:filterchart});
                                                                        //  countries.setParams({styles:'white_border'});
                                                                        var x1 = this.bbox[0];
                                                                        var x2 = this.bbox[1];
                                                                        var x3 = this.bbox[2];
                                                                        var x4 = this.bbox[3];

                                                                        map.fitBounds([
                                                                            [x2, x1],
                                                                            [x4, x3]
                                                                        ])

                                                                        var filterchart = "country_name<>'" + iso2filter + "'";

                                                                        country_mask2.setParams({ CQL_FILTER: filterchart });
                                                                        country_mask2.setParams({ styles: 'white_border' });



                                                                        $('.refresh_legend_' + layer_id + '').empty().append('<i class="material-icons refresh refresh_' + layer_id + '">refresh</i>');
                                                                        // refresh stats after clicckin on bar
                                                                        setTimeout(function() {
                                                                            $('.refresh_' + layer_id + '').click(function() {
                                                                                setTimeout(function() {
                                                                                    $('.bar_chart_' + layer_id + '').click();
                                                                                }, 300);
                                                                                setTimeout(function() {
                                                                                    $('.bar_chart_' + layer_id + '').click();
                                                                                    $('.refresh_legend_' + layer_id + '').empty();
                                                                                }, 300);
                                                                            });
                                                                        }, 1000);
                                                                    }
                                                                }
                                                            }
                                                        }]
                                                    }); //chart
                                                } else {
                                                    $('#chartDiv').hide();
                                                }
                                            }
                                        },
                                    });
                                }, 100);

                            }, 100);
                            $('.all_stats_legend').empty();
                            countries.setOpacity(100);
                        } else {
                            countries.setParams({ styles: 'white_polygon_t' });
                            $('#' + layer_id + '_stats_legend').empty();
                        }
                    });

                }, 100);


            }, 200)
        })


        // Drag Legends -----------------------------------------------
        // dragElement(document.getElementById("legends"));
        // function dragElement(elmnt) {
        //     var pos1 = 0,
        //         pos2 = 0,
        //         pos3 = 0,
        //         pos4 = 0;
        //     if (document.getElementById(elmnt.id + "header")) {
        //         document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
        //     } else {
        //         elmnt.onmousedown = dragMouseDown;
        //     }

        //     function dragMouseDown(e) {
        //         e = e || window.event;
        //         e.preventDefault();
        //         pos3 = e.clientX;
        //         pos4 = e.clientY;
        //         document.onmouseup = closeDragElement;
        //         document.onmousemove = elementDrag;
        //     }

        //     function elementDrag(e) {
        //         e = e || window.event;
        //         e.preventDefault();
        //         pos1 = pos3 - e.clientX;
        //         pos2 = pos4 - e.clientY;
        //         pos3 = e.clientX;
        //         pos4 = e.clientY;
        //         elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        //         elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        //     }

        //     function closeDragElement() {
        //         document.onmouseup = null;
        //         document.onmousemove = null;
        //     }
        // } 
        // END OF DRAG ELEMENT

    } // END OF MAIN BLOCK (ATTACH FUNCTION)
}